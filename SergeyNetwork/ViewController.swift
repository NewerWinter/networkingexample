//
//  ViewController.swift
//  SergeyNetwork
//
//  Created by Aleksandr on 20.02.17.
//  Copyright © 2017 Aleksandr. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {
    
    @IBOutlet weak var button: UIButton! {
        didSet {
            button.addTarget(self, action: #selector(ViewController.buttonClicked(_:)), for: .touchUpInside)
        }
    }
    
    @IBOutlet weak var textView: UITextView!


    func buttonClicked(_ sender: UIButton) {
        let CudaGoRout = API.KudaGo.getConcerts(ctype: "place,date")
        Networking.requestObject(CudaGoRout) { [weak self] (response: Result<KudaGoResult>) in
            switch response {
            case .success(let result):
                guard let concertes = result.concertes else {
                    //Эту проверку можно сделать на уровне модели
                    self?.showAlert(error: NetworkError.noResponse)
                    return
                }
                var text = ""
                concertes.forEach({ (concerte) in
                    text += "ConcertId: \(String(concerte.id))\n\r"
                })
                self?.textView.text = text
                break
            case .failure(let error):
                self?.showAlert(error: error)
            }
           
        };
    }
}


