//
//  NetworkError.swift
//  SergeyNetwork
//
//  Created by Aleksandr on 20.02.17.
//  Copyright © 2017 Aleksandr. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case noResponse
}


extension NetworkError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .noResponse:
            return NSLocalizedString("A user-friendly description of the error.", comment: "My error")
        }
    }
}
