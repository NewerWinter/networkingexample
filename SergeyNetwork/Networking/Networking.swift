//
//  Networking.swift
//  SergeyNetwork
//
//  Created by Aleksandr on 20.02.17.
//  Copyright © 2017 Aleksandr. All rights reserved.
//
import Alamofire
import ObjectMapper
import AlamofireObjectMapper


typealias JSONDictionary = [String: AnyObject]


struct Networking {
    
    static func requestObject<ResultType: Mappable>(_ request: URLRequestConvertible,
                              completion: @escaping (Result<ResultType>) -> Void) {
        let requestDebug = Alamofire.request(request)
            .validate()
            .responseObject { (response: DataResponse<ResultType>) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                    
                case .failure(let error):
                    completion(.failure(error))
                    
                }
        }
        debugPrint(requestDebug)
    }
    
    static func requestArray<ResultType: Mappable>(_ request: URLRequestConvertible,
                             completion: @escaping (Result<[ResultType]>) -> Void) {
        let requestDebug = Alamofire.request(request)
            .validate()
            .responseArray { (response: DataResponse<[ResultType]>) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                    
                case .failure(let error):
                    completion(.failure(error))
                    
                }
        }
        debugPrint(requestDebug)
    }
    
    
}
