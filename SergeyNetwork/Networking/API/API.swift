//
//  API.swift
//  SergeyNetwork
//
//  Created by Aleksandr on 20.02.17.
//  Copyright © 2017 Aleksandr. All rights reserved.
//


import UIKit
import Alamofire


enum API { }


protocol APIMethodProtocol: URLRequestConvertible {
    var method: Alamofire.HTTPMethod { get }
    var path: String { get }
    var params: Alamofire.Parameters? { get }

}


extension APIMethodProtocol {
    
    func asURLRequest() throws -> URLRequest {
        let url = try path.asURL()
        
        var urlRequest = Alamofire.URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        
        switch method {
        case .get:
            return try Alamofire.URLEncoding.default.encode(urlRequest, with: params)
        case .post:
            return try Alamofire.JSONEncoding.default.encode(urlRequest, with: params)
        default:
            break
        }
        return urlRequest
    }
    
}
