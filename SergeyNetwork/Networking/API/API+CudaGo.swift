//
//  AOI+CudaGo.swift
//  SergeyNetwork
//
//  Created by Aleksandr on 20.02.17.
//  Copyright © 2017 Aleksandr. All rights reserved.
//
import Alamofire

extension API {
    enum KudaGo {
        static let url = "https://kudago.com/public-api/v1.3/search"
        case getConcerts(ctype: String)
    }
}

extension API.KudaGo: APIMethodProtocol {
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .getConcerts:
            return .get
        }
    }
    
    var path: String {
        switch self {
        case .getConcerts:
            return API.KudaGo.url
        }
    }
    
    var params: [String: Any]? {
        switch self {
        case .getConcerts(let ctype):
            return ["q": "concert","ctype": "event", "expand": ctype]
        }
}
}
