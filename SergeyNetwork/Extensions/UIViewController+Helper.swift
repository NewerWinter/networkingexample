//
//  UIViewController+Helper.swift
//  SergeyNetwork
//
//  Created by Aleksandr on 20.02.17.
//  Copyright © 2017 Aleksandr. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(error: Error) {
        let alert = UIAlertController(title: "Ошибка", message: error.localizedDescription, preferredStyle: .actionSheet)
        self.present(alert, animated: true, completion: nil)
        
    }
}
