//
//  Concerte.swift
//  SergeyNetwork
//
//  Created by Aleksandr on 20.02.17.
//  Copyright © 2017 Aleksandr. All rights reserved.
//

import ObjectMapper

struct Concerte: Mappable {
    
    var id: Int!
    
    init?(map: Map) {
        //Валидация
        guard let _ = map.JSON["id"] as? Int else {
            return nil
        }
    }
    
    
    mutating func mapping(map: Map) {
        // Преобразование, работает в обе стороны
        id <- map["id"]
    }
}
