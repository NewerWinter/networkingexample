//
//  KudaGoResult.swift
//  SergeyNetwork
//
//  Created by Aleksandr on 20.02.17.
//  Copyright © 2017 Aleksandr. All rights reserved.
//

import ObjectMapper

struct KudaGoResult: Mappable {
    
    var concertes: [Concerte]?
    
    init?(map: Map) {
        //Валидация. Есть альтернативынй протокол с init() throw, позволяет при валидации кидать кастомные ошибки
        guard let _ = map.JSON["results"] else {
            return nil
        }
    }
    
    
    mutating func mapping(map: Map) {
        // Преобразование, работает в обе стороны 
        concertes <- map["results"]
    }
}
